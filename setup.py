from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Books recommendation service.",
    author="AI Talent Hub Students",
    license="MIT",
)
