"""Create original immutable data dump from downloaded files."""

from pathlib import Path

import pandas as pd
from tqdm import tqdm

if __name__ == "__main__":
    DATADIR = "src/data/raw/archive/"
    data = pd.DataFrame()
    for csv in tqdm(Path(DATADIR).glob("*.csv"), desc="Reading CSVs"):
        data = pd.concat([data, pd.read_csv(csv)], ignore_index=True)
    print(data.shape)
    print(data.head())
    print(data.columns)
    data["Rating"] = data["Rating"].astype(str)
    data.to_feather("src/data/raw/books.feather")
