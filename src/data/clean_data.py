from pathlib import Path

import click
import pandas as pd


@click.command()
@click.option(
    "--input-path-1",
    default="data/interim/books_dataset.csv",
    type=click.Path(exists=True),
    help="Path where the intermediate data (filtered by region) "
    "is allocated. Change default if you only need to",
)
@click.option(
    "--input-path-2",
    default="data/raw",
    type=click.Path(exists=True),
    help="Path where the raw data is allocated. Change default " "if you only need to",
)
@click.option(
    "--output-path",
    default="data/processed",
    type=click.Path(exists=True),
    help="Path where the final, clean data is allocated. Change default "
    "if you only need to",
)
def clean_data(input_path_1: str, input_path_2: str, output_path: str):
    """Remove excess columns and enforces.

    Args:
        input_path_1: The path where filtered by region DataFrame is allocated.
        input_path_2: The df limited by train users group.
        output_path: The path to save cleaned DataFrame.

    """
    books_rating = pd.read_csv(input_path_1)
    user_rating = pd.concat(
        [
            pd.read_csv(str(x.resolve()))
            for x in Path(input_path_2).glob("user_rating*.csv")
        ]
    )

    book_id_0 = books_rating[["Name"]]
    book_id_1 = user_rating[["Name"]]
    book_id = pd.concat([book_id_0, book_id_1], axis=0, ignore_index=True)
    book_id.rename(columns={book_id.columns[0]: "Name"}, inplace=True)
    book_id.drop_duplicates(inplace=True)
    book_id["Book_Id"] = book_id.index.values

    user_rating = pd.merge(user_rating, book_id, on="Name", how="left")
    books_rating = pd.merge(books_rating, book_id, on="Name", how="left")
    user_rating.rename(columns={"ID": "User_Id"}, inplace=True)

    user_rating.fillna(0, inplace=True)

    user_rating = user_rating.replace(
        {
            "Rating": {
                "it was amazing": 5,
                "really liked it": 4,
                "liked it": 3.5,
                "it was ok": 3,
                "did not like it": 2,
                "This user doesn't have any rating": 0,
            }
        }
    )

    user_rating = user_rating[user_rating.Rating > 0]

    filter_regex = "[a-zA-Z0-9,#]"
    user_rating_ = user_rating[
        user_rating["Name"].str.contains(filter_regex, regex=True)
    ]

    user_rating_ = user_rating_[user_rating_["Name"].str.match(filter_regex)]

    user_rating_.to_csv(Path(output_path).joinpath("user_rating.csv"), index=False)


if __name__ == "__main__":
    clean_data()  # pylint: disable=no-value-for-parameter
