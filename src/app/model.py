from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass
class Prediction:
    """Prediction dataclass."""

    prediction: list[int]


class Model(ABC):
    """Model abstract class."""

    @abstractmethod
    def __init__(self, model_path: str) -> None:
        """Constructor."""
        raise NotImplementedError

    @abstractmethod
    def predict(self, text: str) -> Prediction:
        """Predict method."""
        raise NotImplementedError


class DummyModel(Model):
    """Dummy model."""

    def __init__(self, model_path: str = "") -> None:
        """Constructor."""
        return None

    def predict(self, text: str) -> Prediction:
        """Predict method."""
        return Prediction(prediction=list(range(10)))
