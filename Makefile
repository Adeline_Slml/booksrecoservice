#.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
PROFILE = default
PROJECT_NAME = BooksRecoService
PYTHON_INTERPRETER = python3
PROJECT = src models

#################################################################################
# COMMANDS                                                                      #
#################################################################################

.venv:
	poetry install --no-root
	poetry check

## Install Python Dependencies by poetry
setup: .venv
	$(PYTHON_INTERPRETER) setup.py install

## Make Dataset
data: requirements
	$(PYTHON_INTERPRETER) src/data/make_dataset.py data/raw data/processed

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete
	rm -rf ".mypy_cache"
	rm -rf ".pytest_cache"
	rm -rf ".venv"

# Format

isort_fix: .venv
	isort $(PROJECT)

## Format code using isort
format: isort_fix

# Lint

isort: .venv
	isort $(PROJECT)

flake: .venv
	flake8 $(PROJECT)

mypy: .venv
	mypy $(PROJECT) 

pylint: .venv
	pylint $(PROJECT)

# Test

.pytest:
	pytest

test: .venv .pytest

## Lint using flake8 and other linters
lint: isort flake mypy pylint

## Run all
all: clean setup lint

## Test python environment is setup correctly
test_environment:
	$(PYTHON_INTERPRETER) test_environment.py
