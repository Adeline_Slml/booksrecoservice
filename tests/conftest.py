import pytest
from numpy.typing import NDArray
import numpy as np


@pytest.fixture
def recon_batch() -> NDArray[np.float32]:
    return np.array(
        [
            [0.9, 0.2, 0.3, 0.4, 0.5, 0.6],
            [0.7, 0.3, 0.1, 0.4, 0.8, 0.9],
        ],
        dtype=np.float32,
    )


@pytest.fixture
def true_batch() -> NDArray[np.float32]:
    return np.array(
        [
            [0.0, 0.0, 0.0, 0.0, 0.5, 0.6],
            [0.9, 0.0, 0.6, 0.3, 0.0, 0.7],
        ],
        dtype=np.float32,
    )
