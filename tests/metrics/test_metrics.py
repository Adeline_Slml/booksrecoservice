import numpy as np
import pytest

from models.autoencoders.tools.metrics import (
    ap_at_k,
    precision_at_k,
    recall_at_k,
    rr_at_k,
)


@pytest.mark.parametrize(
    "k, expected",
    [
        (1, np.array([0.0, 1.0], dtype=np.float32)),
        (2, np.array([0.5, 0.5], dtype=np.float32)),
        (3, np.array([1.0, 2 / 3], dtype=np.float32)),
        (4, np.array([1.0, 0.75], dtype=np.float32)),
        (5, np.array([1.0, 0.75], dtype=np.float32)),
        (6, np.array([1.0, 1.0], dtype=np.float32)),
    ],
)
def test_recall_at_k(recon_batch, true_batch, k, expected) -> None:
    assert (recall_at_k(recon_batch, true_batch, k) == expected).all()


@pytest.mark.parametrize(
    "k, expected",
    [
        (1, np.array([0.0, 1.0], dtype=np.float32)),
        (2, np.array([0.5, 0.5], dtype=np.float32)),
        (3, np.array([2 / 3, 2 / 3], dtype=np.float32)),
        (4, np.array([0.5, 0.75], dtype=np.float32)),
        (5, np.array([0.4, 0.6], dtype=np.float32)),
        (6, np.array([1 / 3, 2 / 3], dtype=np.float32)),
    ],
)
def test_precision_at_k(recon_batch, true_batch, k, expected) -> None:
    assert (precision_at_k(recon_batch, true_batch, k) == expected).all()


@pytest.mark.parametrize(
    "k, expected",
    [
        (1, np.array([0.0, 1.0], dtype=np.float32)),
        (2, np.array([0.5, 1.0], dtype=np.float32)),
        (3, np.array([0.5, 1.0], dtype=np.float32)),
        (5, np.array([0.5, 1.0], dtype=np.float32)),
        (4, np.array([0.5, 1.0], dtype=np.float32)),
        (6, np.array([0.5, 1.0], dtype=np.float32)),
    ],
)
def test_mrr_at_k(recon_batch, true_batch, k, expected) -> None:
    assert (rr_at_k(recon_batch, true_batch, k) == expected).all()


@pytest.mark.parametrize(
    "k, expected",
    [
        (1, np.array([0.0, 1.0], dtype=np.float32)),
        (2, np.array([0.25, 0.5], dtype=np.float32)),
        (3, np.array([7 / 12, 5 / 9], dtype=np.float32)),
        (4, np.array([7 / 12, 29 / 48], dtype=np.float32)),
        (5, np.array([7 / 12, 29 / 48], dtype=np.float32)),
        (6, np.array([7 / 12, 37 / 48], dtype=np.float32)),
    ],
)
def test_ap_at_k(recon_batch, true_batch, k, expected) -> None:
    assert np.allclose(
        ap_at_k(recon_batch, true_batch, k),
        expected,
    )
