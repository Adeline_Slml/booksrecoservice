import pytest

from src.app.model import DummyModel


@pytest.fixture
def model() -> DummyModel:
    """Fixture for dummy model."""
    return DummyModel()


def test_dummy_model(model: DummyModel) -> None:
    """Test dummy model."""
    prediction = model.predict("test")
    assert prediction.prediction == list(range(10))
