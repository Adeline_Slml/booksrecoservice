from collections.abc import Generator

import numpy as np
from numpy.typing import NDArray

#################################################################################
# ACCURACY METRICS                                                              #
#################################################################################


def mean_recall_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> float:
    """Wrapper for recall_at_k to calculate the mean recall@K metric for batch."""
    return np.mean(recall_at_k(recon_batch, true_batch, k)).item()


def recall_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> NDArray[np.float32]:
    """Calculate recall@K for users in batch.

    Recall at k is the proportion of relevant items found in the top-k recommendations.

    Args:
        recon_batch: The batch of recos that model built for each user.
        true_batch: The batch thath contains only relevant items for each user.
        k: The number of recos which are considered.

    Returns:
        recall_k: The batch of recall@K.

    """
    batch_users = recon_batch.shape[0]

    # Indexes of the first k recommendations
    idx = np.argpartition(-recon_batch, k - 1, axis=1)

    # Boolean mask where True means this item is in recos at k
    recon_batch_bool = np.zeros_like(recon_batch, dtype=bool)
    recon_batch_bool[np.arange(batch_users)[:, np.newaxis], idx[:, :k]] = True

    true_batch_bool = true_batch > 0

    # Number of the relevant items that model recommends at k for each user in batch
    intersection_cardinality: NDArray[np.float32] = (
        (recon_batch_bool & true_batch_bool).sum(axis=1).astype(np.float32)
    )

    # Calculate the batch of Recall@K by definition
    divider: NDArray[np.int64] = np.minimum(k, true_batch_bool.sum(axis=1))
    recall_k = (intersection_cardinality / divider).astype(np.float32)
    return recall_k


def mean_precision_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> float:
    """Wrapper for precision_at_k to calculate the mean precision@K metric for batch."""
    return np.mean(precision_at_k(recon_batch, true_batch, k)).item()


def precision_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> NDArray[np.float32]:
    """Calculate precision@K for users in batch.

    Precision at k is the proportion of recommended items in the top-k set
    that are relevant.

    Args:
        recon_batch: The batch of recos that model built for each user.
        true_batch: The batch thath contains only relevant items for each user.
        k: The number of recos which are considered.

    Returns:
        precision_k: The batch of precision@K.

    """
    batch_users = recon_batch.shape[0]

    # Indexes of the first k recommendations
    recos_ids: NDArray[np.intp] = np.argpartition(-recon_batch, k - 1, axis=1)

    # Boolean mask where True means this item is in recos at k
    is_item_in_recos = np.zeros_like(recon_batch, dtype=bool)
    is_item_in_recos[np.arange(batch_users)[:, np.newaxis], recos_ids[:, :k]] = True

    true_batch_bool = true_batch > 0

    # Number of the relevant items that model recommends at k for each user in batch
    intersection_cardinality: NDArray[np.float32] = (
        (is_item_in_recos & true_batch_bool).sum(axis=1).astype(np.float32)
    )

    # Calculate the batch of Precision@K by definition
    precision_k = (intersection_cardinality / k).astype(np.float32)
    return precision_k


#################################################################################
# RANKING METRICS                                                               #
#################################################################################


def mrr_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> float:
    """Wrapper for rr_at_k to calculate the mean reciprocal rank metric for batch."""
    return np.mean(rr_at_k(recon_batch, true_batch, k)).item()


def rr_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> NDArray[np.float32]:
    """Calculate the reciprocal ranks for users in batch.

    The reciprocal rank of top-k recommendations is the multiplicative inverse
    of the rank of the first relevant item occurence in this list: 1 for first place,
    1/2 for second place, 1/3 for third place and so on.

    Args:
        recon_batch: The batch of recos that model built for each user.
        true_batch: The batch thath contains only relevant items for each user.
        k: The number of recos which are considered.

    Returns:
        rr: The batch of reciprocal ranks.

    """
    batch_users = recon_batch.shape[0]

    # Indexes of the first k recommendations
    recos_ids: NDArray[np.int64] = np.argpartition(-recon_batch, k - 1, axis=1)

    # Get recos ids in right sorted order (from high to low)
    top_k_recs = recon_batch[np.arange(batch_users)[:, np.newaxis], recos_ids[:, :k]]
    sorted_recos_ids: NDArray[np.int64] = recos_ids[
        np.arange(batch_users)[:, np.newaxis], np.argsort(-top_k_recs)
    ]

    # Boolean mask where relevant recos are True values
    true_batch_bool = true_batch > 0
    indicator_k = true_batch_bool[
        np.arange(batch_users)[:, np.newaxis], sorted_recos_ids
    ]

    # Index of the first relevant reco for each user
    relevant_rank: Generator[NDArray[np.int64], None, None] = (
        np.nonzero(row == 1)[0] for row in indicator_k
    )

    # Calculate the batch of RR@K by definition
    rr_list = [1.0 / (arr[0] + 1) if arr.size else 0.0 for arr in relevant_rank]
    rr = np.array(rr_list).astype(np.float32)
    return rr


def map_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> float:
    """Wrapper for ap_at_k to calculate the mean average precision metric for batch."""
    return np.mean(ap_at_k(recon_batch, true_batch, k)).item()


def ap_at_k(
    recon_batch: NDArray[np.float32], true_batch: NDArray[np.float32], k: int
) -> NDArray[np.float32]:
    """Calculate the average precision for each user in batch.

    The average precision of the top-k recommendations is the sum of precision@K
    where the item at the kth rank is relevant divided by the total number of
    relevant items (with adjustment to k) in the top-k recommendations.

    Args:
        recon_batch: The batch of recos that model built for each user.
        true_batch: The batch thath contains only relevant items for each user.
        k: The number of recos which are considered.

    Returns:
        ap: The batch of AP@K.

    """
    batch_users = recon_batch.shape[0]

    true_batch_bool = true_batch > 0
    # Indexes of the first k recommendations
    recos_ids: NDArray[np.int64] = np.argpartition(-recon_batch, k - 1, axis=1)
    # Get recos ids in right sorted order (from high to low)
    top_k_recs = recon_batch[np.arange(batch_users)[:, np.newaxis], recos_ids[:, :k]]
    sorted_recos_ids: NDArray[np.int64] = recos_ids[
        np.arange(batch_users)[:, np.newaxis], np.argsort(-top_k_recs)
    ]

    # Boolean mask where relevant recos are True values
    indicator_k = true_batch_bool[
        np.arange(batch_users)[:, np.newaxis], sorted_recos_ids
    ]

    # Accumulate precisions
    precisions = np.zeros_like(top_k_recs)
    for running_k in range(1, k + 1):
        # Calculate precision in a familiar way
        is_item_in_recos = np.zeros_like(recon_batch, dtype=bool)
        is_item_in_recos[
            np.arange(batch_users)[:, np.newaxis], sorted_recos_ids[:, :running_k]
        ] = True

        intersection_cardinality: NDArray[np.float32] = (
            (is_item_in_recos & true_batch_bool)
            .sum(axis=1, keepdims=True)
            .astype(np.float32)
        )

        precision_k = (intersection_cardinality / running_k).astype(np.float32)
        # Update values
        precisions[np.arange(batch_users)[:, np.newaxis], running_k - 1] = precision_k

    # Calculate the batch of AP@K by definition
    divider: NDArray[np.int64] = np.minimum(k, true_batch_bool.sum(axis=1))
    ap = ((precisions * indicator_k).sum(axis=1) / divider).astype(np.float32)
    return ap
