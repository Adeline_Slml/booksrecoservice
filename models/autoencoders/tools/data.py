"""All functions and classes to get the data pipeline done."""
import os
from math import ceil

import numpy as np
import pandas as pd
import torch
from numpy.typing import NDArray
from pandera.typing import Series
from scipy.sparse import coo_matrix, csr_matrix, vstack

pd.options.mode.chained_assignment = None


class Columns:
    """Aliases for column names."""

    Name = "item_name"
    User = "user_id"
    Item = "item_id"
    Weight = "weight"


class BookTrainDataset(torch.utils.data.Dataset):
    """Convert train data to sparse format.

    In order to train autoencoder model, there should be a matrix, where
    rows represent users, columns - items and rating sits at intersection
    of specific user_id and item_id.

    Attributes:
        dir: The directory path where the data are allocated.
        n_books: The number of all books.
        matrix: The train interactions matrix as
            input for auetoencoders models.

    """

    def __init__(self, path: str):
        super().__init__()
        self.dir = path
        self.n_books = self._get_n_books()
        self.matrix = self._extract_train_data()
        self.shape: tuple[int, int] = self.matrix.get_shape()

    @property
    def sparsity(self) -> float:
        """Get the sparsity of train matrix."""
        return self.matrix.getnnz() / (self.shape[0] * self.shape[1])

    def _get_n_books(self) -> int:
        path = os.path.join(self.dir, "items_count.txt")
        with open(path, "r", encoding="utf8") as f:
            n_books = int(f.readline())
        return n_books

    def _extract_train_data(self) -> csr_matrix:
        path = os.path.join(self.dir, "train.csv")

        train_interactions = pd.read_csv(path)
        ratings = train_interactions[Columns.Weight].values

        n_users = len(train_interactions.groupby(Columns.User).size())

        rows, cols = (
            train_interactions[Columns.User].values,
            train_interactions[Columns.Item].values,
        )
        data = csr_matrix(
            (ratings, (rows, cols)),
            dtype="float64",
            shape=(n_users, self.n_books),
        )
        return data

    def __len__(self) -> int:
        """Return the length of dataset."""
        return self.shape[0]

    def __getitem__(self, idx) -> int:
        """Return one item from dataset by index."""
        return self.matrix[idx]


class BookTestDataset(torch.utils.data.Dataset):
    """Convert test data to sparse format.

    In order to train autoencoder model, there should be a matrix, where
    rows represent users, columns - items and rating sits at intersection
    of specific user_id and item_id.

    The only difference from train data is that we should store 2 matrixes:
    one as input for model to build reconstruction and other as validation
    part for metrics calculation.

    Attributes:
        dir: The directory path where the data are allocated.
        n_books: The number of all books.
        matrix_input: The test interactions matrix (sparse view) as input
            for auetoencoders models to build reconstruction.
        matrix_valid: The test interactions matrix (sparse view) as valididation
            to compare with reconstruction results.

    """

    def __init__(self, path: str):
        super().__init__()
        self.dir = path
        self.n_books = self._get_n_books()
        self.matrix_input, self.matrix_valid = self._extract_test_data()
        self.shape_input: tuple[int, int] = self.matrix_input.get_shape()
        self.shape_valid: tuple[int, int] = self.matrix_valid.get_shape()

    @property
    def sparsity(self) -> tuple[float, float]:
        """Get the sparsity of both test matrices."""
        return self.matrix_input.getnnz() / (
            self.shape_input[0] * self.shape_input[1]
        ), self.matrix_valid.getnnz() / (self.shape_valid[0] * self.shape_valid[1])

    def _get_n_books(self) -> int:
        path = os.path.join(self.dir, "items_count.txt")
        with open(path, "r", encoding="utf8") as f:
            n_books = int(f.readline())
        return n_books

    def _extract_test_data(self) -> tuple[csr_matrix, csr_matrix]:
        path_input = os.path.join(self.dir, "test_input.csv")
        path_valid = os.path.join(self.dir, "test_valid.csv")

        # Have equal number of users
        test_interactions_input = pd.read_csv(path_input)
        test_interactions_valid = pd.read_csv(path_valid)

        ratings_input = test_interactions_input[Columns.Weight].values
        ratings_valid = test_interactions_valid[Columns.Weight].values

        # Reindex to meet dimension boundaries
        start_idx = test_interactions_input[Columns.User].min()
        end_idx = test_interactions_input[Columns.User].max()
        n_users = end_idx - start_idx + 1

        rows_input, cols_input = (
            test_interactions_input[Columns.User].values - start_idx,
            test_interactions_input[Columns.Item].values,
        )
        rows_valid, cols_valid = (
            test_interactions_valid[Columns.User].values - start_idx,
            test_interactions_valid[Columns.Item].values,
        )

        data_input = csr_matrix(
            (ratings_input, (rows_input, cols_input)),
            dtype="float64",
            shape=(n_users, self.n_books),
        )

        data_valid = csr_matrix(
            (ratings_valid, (rows_valid, cols_valid)),
            dtype="float64",
            shape=(n_users, self.n_books),
        )

        return data_input, data_valid

    def __len__(self) -> int:
        """Return the length of dataset."""
        return self.shape_valid[0]  # Both matrices have the same len

    def __getitem__(self, idx) -> tuple[int, int]:
        """Return pair of items from dataset by index."""
        return (
            self.matrix_input[idx],
            self.matrix_valid[idx],
        )


def coo_matrix_to_sparse_tensor(coo: coo_matrix) -> torch.Tensor:
    """Transform scipy coo matrix to pytorch sparse tensor."""
    values = coo.data
    indices = np.vstack((coo.row, coo.col))
    shape = coo.shape

    ind = torch.LongTensor(indices)
    val = torch.FloatTensor(values)
    shp = torch.Size(shape)

    return torch.sparse_coo_tensor(ind, val, shp)


def train_sparse_collate_fn(batch: list[csr_matrix]) -> torch.Tensor:
    """Collate function which transform train scipy coo matrix to pytorch tensor."""
    batch_coo: coo_matrix = vstack(batch).tocoo()
    torch_sparse_batch = coo_matrix_to_sparse_tensor(batch_coo)
    return torch_sparse_batch.to_dense()


def test_sparse_collate_fn(
    batch: list[tuple[csr_matrix, csr_matrix]]
) -> tuple[torch.Tensor, torch.Tensor]:
    """Collate function which transform scipy test coo matrixes to pytorch tensor."""
    batch_input_coo: coo_matrix = vstack([item[0] for item in batch]).tocoo()
    batch_valid_coo: coo_matrix = vstack([item[1] for item in batch]).tocoo()
    torch_sparse_batch_input = coo_matrix_to_sparse_tensor(batch_input_coo)
    torch_sparse_batch_valid = coo_matrix_to_sparse_tensor(batch_valid_coo)
    return torch_sparse_batch_input.to_dense(), torch_sparse_batch_valid.to_dense()


def count_per_item(interactions: pd.DataFrame, column_name: str) -> Series[int]:
    """Get a dataframe containing size of groups formed by specific column."""
    item_groups = interactions.groupby([column_name], as_index=False).size()
    return item_groups


def filter_by_limits(
    interactions: pd.DataFrame, user_lim: int, item_lim: int
) -> tuple[pd.DataFrame, Series[int], Series[int]]:
    """Filter over the dataframe iteratively until nothing could be removed.

    Args:
        interactions: The dataframe containing interactions.
        user_lim: The mininum number of rated items that one user must have.
        item_lim: The mininum number of ratings that one item must have.

    Returns:
        Returns a tuple (interactions, user_groups, item_groups), where interactions is
            filtered dataframe, user_groups is dataframe containing number of books per
                user, item_groups is dataframe containing number of ratings per book.

    """
    while True:
        end_cycle = False
        if user_lim > 0:
            user_groups = count_per_item(interactions, Columns.User)
            n_users = len(user_groups)
            mask = interactions[Columns.User].isin(
                user_groups[user_groups["size"] >= user_lim][Columns.User].values
            )
            interactions = interactions[mask]
            # Check if smth was removed in this step
            if interactions[Columns.User].nunique() == n_users:
                end_cycle = True
        if item_lim > 0:
            item_groups = count_per_item(interactions, Columns.Item)
            n_books = len(item_groups)
            mask = interactions[Columns.Item].isin(
                item_groups[item_groups["size"] >= item_lim][Columns.Item].values
            )
            interactions = interactions[mask]

            if end_cycle and interactions[Columns.Item].nunique() == n_books:
                break

    print(f"{interactions[Columns.User].nunique()} unique users left", end="\n")
    print(f"{interactions[Columns.Item].nunique()} unique books left")

    user_groups, item_groups = count_per_item(
        interactions, Columns.User
    ), count_per_item(interactions, Columns.Item)
    return interactions, user_groups, item_groups


def reindex_mappings(
    train_interactions: pd.DataFrame,
    test_interactions: pd.DataFrame,
    items: NDArray[np.float64],
) -> tuple[dict[int, int], dict[int, int]]:
    """Create the mappings for each dataframe.

    Args:
        train_interactions: The training dataframe containing interactions.
        test_interactions: The testing dataframe containing interactions.
        items: The unique item ids.

    Returns:
        Returns a tuple (user_to_ind, item_to_ind), where user_to_ind is dictionary
            containing mappings from external user ids to the internal, user_to_ind is
                dictionary containing mappings from external item ids to the internal.

    """
    users_id: NDArray[np.int64] = train_interactions[Columns.User].values
    user_to_ind: dict[int, int] = {}

    for user in users_id:
        if user not in user_to_ind:
            user_to_ind[user] = len(user_to_ind)

    users_id = test_interactions[Columns.User].values
    for user in users_id:
        if user not in user_to_ind:
            user_to_ind[user] = len(user_to_ind)

    item_to_ind: dict[int, int] = {}
    for item in items:
        item_to_ind[item] = len(item_to_ind)

    return user_to_ind, item_to_ind


def apply_reindex(
    interactions: pd.DataFrame, user_to_ind: dict[int, int], item_to_ind: dict[int, int]
) -> None:
    """
    Remap users and items ids using mappings.

    Args:
        interactions: The dataframe containing interactions.
        user_to_ind: The mapping from external user ids to the internal.
        item_to_ind: The mapping from external item ids to the internal.

    """
    interactions[Columns.User] = interactions[Columns.User].apply(
        lambda x: user_to_ind[x]
    )
    interactions[Columns.Item] = interactions[Columns.Item].apply(
        lambda x: item_to_ind[x]
    )


def split_test_data(
    test_interactions: pd.DataFrame, validation_size: float, relevance_trsh: float
) -> tuple[pd.DataFrame, pd.DataFrame, list]:
    """Split items per each user into 2 groups.

    One part goes into an autoencoder as the input for building reconstruction first,
    then another part will be used for metrics calculation along with reconstruction
    results, and the latter consists only of relevance items to make it meaningful.
    If it's impossible to split user items into 2 categories under these terms, then
    he is removed from test set.

    Args:
        test_interactions: The interactions limited by test users group.
        validation_size: The proportion of data that will be left to validation (from 0
            to 1). Would be converted to int representing the absolute number of items.
        relevance_trsh: The treshold rating value above which items are considering
            relevant for user.

    Returns:
        Returns a tuple (interactions_input, interactions_valid, rejected_test_users),
            where the interactions_input is the test_interactions without items sent to
                validation, the interactions_valid is the one containing it and
                    rejected_test_users contains indexes of users to be removed.

    """
    user_groups = test_interactions.groupby(Columns.User)
    rejected_test_users = []
    list_input, list_valid = [], []

    for _, group in user_groups:
        n_user_items = len(group)
        user_rel_ratings: NDArray[np.int64] = (
            group[Columns.Weight].values >= relevance_trsh
        ).nonzero()[0]
        n_rel_user_items = len(user_rel_ratings)

        # Check if user has the proper number of relevance items
        n_rel_needed = ceil(validation_size * n_user_items)
        if n_rel_needed == n_user_items or n_rel_needed > n_rel_user_items:
            rejected_test_users.append(group[Columns.User].iloc[0])
            continue
        idx = np.zeros(n_user_items, dtype="bool")
        idx[
            np.random.choice(
                user_rel_ratings,
                size=n_rel_needed,
                replace=False,
            )
        ] = True
        list_valid.append(group[idx])
        list_input.append(group[~idx])
    interactions_input = pd.concat(list_input)
    interactions_valid = pd.concat(list_valid)

    return (
        interactions_input,
        interactions_valid,
        rejected_test_users,
    )
