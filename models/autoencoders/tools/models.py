import numpy as np
import torch
import torch.nn.functional as F
from torch import nn


class LinearAE(nn.Module):
    """The realization of a simple linear autoencoder using PyTorch.

    Attributes:
        enc_dims: The list containing dimension sizes of encoder layers.
        dec_dims: The list containing dimension sizes of decoder layers.

    """

    def __init__(
        self,
        enc_dims: list[int],
        dec_dims: list[int] | None = None,
    ):
        super().__init__()
        self.enc_dims = enc_dims
        self.dec_dims = dec_dims
        if dec_dims:
            if enc_dims[0] != dec_dims[-1]:
                raise ValueError("Reconstruction dimension should be equal to input")
            if enc_dims[-1] != dec_dims[0]:
                raise ValueError(
                    "Latent dimension for encoder and decoder should be equal"
                )
            self.dec_dims = dec_dims
        else:
            # Decoder is symmetric by default
            self.dec_dims = enc_dims[::-1]

        self.dims = self.enc_dims + self.dec_dims[1:]
        self.layers = nn.ModuleList(
            [
                nn.Linear(d_in, d_out)
                for d_in, d_out in zip(self.dims[:-1], self.dims[1:])
            ]
        )

    @property
    def device(self):
        """Move all model parameters to the device."""
        return next(self.parameters()).device

    def forward(self, batch: torch.Tensor) -> torch.Tensor:
        """Pass data batch through all layers."""
        # Row-wise L2 norm is meant to take user's rating behaviour into account
        output = F.normalize(batch)

        for layer in self.layers:
            output = layer(output)
        return output

    def predict(self, batch: torch.Tensor) -> torch.Tensor:
        """Get prediction while inference."""
        return self.forward(batch)


class DenoisingAE(nn.Module):
    """The realization of a denoising autoencoder using PyTorch.

    Attributes:
        enc_dims: The list containing dimension sizes of encoder layers.
        dec_dims: The list containing dimension sizes of decoder layers.
        corrupt_ratio: The denoising parameter, i.e. the proportion of
            data that will be corrupted.

    """

    def __init__(
        self,
        enc_dims: list[int],
        dec_dims: list[int] | None = None,
        corrupt_ratio: float = 0.2,
    ):
        super().__init__()
        if not 0 < corrupt_ratio < 1:
            raise ValueError(
                f"corrupt_ratio={corrupt_ratio} should be a float in (0, 1) range."
            )

        self.enc_dims = enc_dims
        if dec_dims:
            if enc_dims[0] != dec_dims[-1]:
                raise ValueError("Reconstruction dimension should be equal to input")
            if enc_dims[-1] != dec_dims[0]:
                raise ValueError(
                    "Latent dimension for encoder and decoder should be equal"
                )
            self.dec_dims = dec_dims
        else:
            # Decoder is symmetric by default
            self.dec_dims = enc_dims[::-1]

        self.dims = self.enc_dims + self.dec_dims[1:]
        self.layers = nn.ModuleList(
            [
                nn.Linear(d_in, d_out)
                for d_in, d_out in zip(self.dims[:-1], self.dims[1:])
            ]
        )

        self.corrupt_ratio = corrupt_ratio
        self._init_weights()

    def _init_weights(self) -> None:
        for layer in self.layers:
            # Xavier Initialization
            size = layer.weight.size()
            fan_out = size[0]
            fan_in = size[1]
            scope = np.sqrt(6.0 / (fan_in + fan_out))
            layer.weight.data.uniform_(-scope, scope)
            layer.bias.data.zero_()

    @property
    def device(self):
        """Move all model parameters to the device."""
        return next(self.parameters()).device

    def forward(self, batch: torch.Tensor) -> torch.Tensor:
        """Pass data batch through all layers."""
        # Row-wise L2 norm is meant to take user's rating behaviour into account
        output = F.normalize(batch)
        # Corrupt the interactions matrix only during training
        output = F.dropout(batch, p=self.corrupt_ratio, training=self.training)

        for i, layer in enumerate(self.layers):
            output = layer(output)
            # Apply activation except the last decoding layer
            if i != len(self.layers) - 1:
                output = torch.tanh(output)
        return output

    def predict(self, batch: torch.Tensor) -> torch.Tensor:
        """Get prediction while inference."""
        return self.forward(batch)


class VariationalAE(nn.Module):
    """The realization of a variational autoencoder using PyTorch.

    Variational autoencoder based on multinomial likelihood with noising
    potentially making it robust. It was designed for implicit feedback
    and so obtaining good results with explicit feedback, i.e. clear ratings,
    are not guaranteed.

    Attributes:
        enc_dims: The list containing dimension sizes of encoder layers.
        dec_dims: The list containing dimension sizes of decoder layers.
        corrupt_ratio: The denoising parameter, i.e. the proportion of
            data that will be corrupted.

    """

    def __init__(
        self,
        enc_dims: list[int],
        dec_dims: list[int] | None = None,
        corrupt_ratio: float = 0.2,
    ):
        super().__init__()
        if not 0 < corrupt_ratio < 1:
            raise ValueError(
                f"corrupt_ratio={corrupt_ratio} should be a float in (0, 1) range."
            )

        self.enc_dims = enc_dims
        if dec_dims:
            if enc_dims[0] != dec_dims[-1]:
                raise ValueError("Reconstruction dimension should be equal to input")
            if enc_dims[-1] != dec_dims[0]:
                raise ValueError(
                    "Latent dimension for encoder and decoder should be equal"
                )
            self.dec_dims = dec_dims
        else:
            # Decoder is symmetric by default
            self.dec_dims = enc_dims[::-1]

        # Create dimension to store the params of variational distribution
        enc_dims_ext = self.enc_dims[:-1] + [self.enc_dims[-1] * 2]
        self.enc_layers = nn.ModuleList(
            [
                nn.Linear(d_in, d_out)
                for d_in, d_out in zip(enc_dims_ext[:-1], enc_dims_ext[1:])
            ]
        )
        self.dec_layers = nn.ModuleList(
            [
                nn.Linear(d_in, d_out)
                for d_in, d_out in zip(self.dec_dims[:-1], self.dec_dims[1:])
            ]
        )

        self.corrupt_ratio = corrupt_ratio
        self._init_weights()

    def _init_weights(self) -> None:
        for layer in self.enc_layers + self.dec_layers:
            # Xavier Initialization
            size = layer.weight.size()
            fan_out = size[0]
            fan_in = size[1]
            scope = np.sqrt(6.0 / (fan_in + fan_out))
            layer.weight.data.uniform_(-scope, scope)
            layer.bias.data.zero_()

    @property
    def device(self):
        """Move all model parameters to the device."""
        return next(self.parameters()).device

    def forward(
        self, batch: torch.Tensor
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        """Build data reconstruction using latent representation."""
        mu, logvar = self.encode(batch)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

    def predict(self, batch: torch.Tensor) -> torch.Tensor:
        """Get prediction while inference."""
        mu, _ = self.encode(batch)
        return self.decode(mu)

    def encode(self, batch: torch.Tensor) -> tuple[torch.Tensor, torch.Tensor]:
        """Obtain params of the variational distribution."""
        # Row-wise L2 norm is meant to take user's rating behaviour into account
        output = F.normalize(batch)
        # Corrupt the interactions matrix only during training
        output = F.dropout(batch, p=self.corrupt_ratio, training=self.training)

        for i, layer in enumerate(self.enc_layers):
            output = layer(output)
            # Apply activation except the last layer
            if i != len(self.enc_layers) - 1:
                output = torch.tanh(output)
            else:
                mu = output[:, : self.enc_dims[-1]]
                logvar = output[:, self.enc_dims[-1] :]
        return mu, logvar

    def reparameterize(self, mu: torch.Tensor, logvar: torch.Tensor) -> torch.Tensor:
        """Obtain a latent vector by applying a reparametrization trick."""
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu

    def decode(self, z: torch.Tensor) -> torch.Tensor:
        """Build reconstruction from the latent representation."""
        output = z
        for i, layer in enumerate(self.dec_layers):
            output = layer(output)
            if i != len(self.dec_layers) - 1:
                output = torch.tanh(output)
        return output
