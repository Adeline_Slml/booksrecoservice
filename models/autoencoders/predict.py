import os
import pickle
from typing import Literal, TypeAlias

import click
import numpy as np
import pandas as pd
import tools
import torch
from numpy.typing import NDArray
from tools.data import Columns
from tools.models import DenoisingAE, LinearAE, VariationalAE

Model: TypeAlias = DenoisingAE | LinearAE | VariationalAE


#################################################################################
# INFERENCE CODE                                                                #
#################################################################################


def create_user_input(
    input_dim: int,
    interactions: pd.DataFrame,
    user_id: int,
    item_to_ind: dict[int, int],
) -> torch.Tensor:
    """Build proper user input for the autoencoder model.

    Extract interactions from data for the specific user id (the existence of
    the data is meant) and build input for the autoencoder model: tensor of
    shape (1, input_dim) filled with floats where 0.0 means absence of interaction.

    Args:
        input_dim: The input dimension of the model.
        interactions: The dataframe containing interactions.
        user_id: The external user id.
        item_to_ind: The mapping from external item ids to the internal.

    Returns:
        user_input: The user input for autoencoder model.

    """
    cond = interactions[Columns.User] == user_id
    items: NDArray[np.int64] = interactions[cond][Columns.Item].values
    ratings: NDArray[np.float64] = interactions[cond][Columns.Weight].values

    user_input = torch.zeros(size=(1, input_dim), dtype=torch.float32)
    items = np.vectorize(item_to_ind.get)(items)

    # Complete user input
    ratings_tsr = torch.tensor(ratings, dtype=torch.float32)
    user_input[:, items] = ratings_tsr
    return user_input


def predict_for_user_id(
    user_input: torch.Tensor,
    model: Model,
    k: int,
    ind_to_item: dict[int, int],
    filter_viewed: bool = True,
) -> NDArray[np.int64]:
    """Get top-k recommendations for one user.

    Args:
        user_input: The user input for autoencoder model.
        model: The allowed PyTorch model in evaluation regime.
        k: The number of recos which are considered.
        ind_to_item: The mapping from internal item ids to the external.
        filter_viewed: The flag to get rid of watched items in recos or not.

    Returns:
        recos_at_k: The top-k recommendations for user as external item indices.

    """
    # Get recos_at_k
    recon: NDArray[np.float32] = model.predict(user_input).detach().to("cpu").numpy()
    if filter_viewed:
        recon[user_input.numpy().nonzero()] = float("-inf")
    recos: NDArray[np.int64] = np.argpartition(-recon, k, axis=1)
    recos_top_k = recos[:, :k]
    # Internal indices to external
    recos_top_k = np.vectorize(ind_to_item.get)(recos_top_k)
    return recos_top_k.squeeze()


#################################################################################
# INFERENCE                                                                     #
#################################################################################


@click.command()
@click.pass_context
@click.option(
    "--input-modelpath",
    default="models/autoencoders/model",
    show_default=True,
    type=click.Path(exists=True),
    help="Path where saved model data are allocated",
    required=True,
)
@click.option(
    "--input-datapath",
    default="models/autoencoders/data",
    show_default=True,
    type=click.Path(exists=True),
    help="Path where saved data for training & inference is allocated. Change default "
    "if you only specified another one during data creation",
)
@click.option(
    "--user-id",
    type=int,
    help="User id you want to predict to. The list of possible user ids can be found "
    "in saved file 'inference_useres.txt' in the data",
    required=True,
)
@click.option(
    "--num-recos",
    "k",
    default=15,
    type=int,
    show_default=True,
    help="Number of recos to give user",
)
@click.option(
    "--cuda",
    is_flag=True,
    default=False,
    show_default=True,
    help="Execution device. Don't specify --cuda if cpu is preffered",
)
def main(
    ctx: click.Context,
    input_modelpath: str,
    input_datapath: str,
    user_id: int,
    k: int,
    cuda: bool,
) -> None:
    """Run inference pipeline."""
    # Upload interactions table suitable (proper item ids) for inference
    interactions = pd.read_csv(os.path.join(input_datapath, "inference.csv"), header=0)

    interactions.rename(
        columns={
            "User_Id": Columns.User,
            "Book_Id": Columns.Item,
            "Name": Columns.Name,
            "Rating": Columns.Weight,
        },
        inplace=True,
    )
    # Handling errors
    items_data = interactions[interactions[Columns.User] == user_id][
        Columns.Item
    ].values
    if not items_data.size:
        raise ValueError("No interactions data for the given user id. Try another one.")

    # Upload the mapper
    with open(os.path.join(input_datapath, "item_to_ind"), "rb") as fp:
        item_to_ind = pickle.load(fp)
    ind_to_item = {v: k for k, v in item_to_ind.items()}  # Reverse mapping

    # Set device
    if torch.cuda.is_available() and not cuda:
        print(
            "WARNING: You have a CUDA device, so you should probably run with --cuda."
        )
    elif not torch.cuda.is_available() and cuda:
        print("WARNING: You don't have a CUDA device, so the program will run on cpu.")
    device = torch.device("cuda" if cuda and torch.cuda.is_available() else "cpu")

    # Get dimensions size of the given autoencoder model
    input_dimspath = os.path.join(input_modelpath, "dims.pickle")
    with open(input_dimspath, "rb") as fp:
        enc_dims: list[int] = pickle.load(fp)

    input_namepath = os.path.join(input_modelpath, "name.pickle")
    with open(input_namepath, "rb") as fp:
        model_name: Literal["DenoisingAE", "LinearAE", "VariationalAE"] = pickle.load(
            fp
        )

    # Initialize model
    model: Model = getattr(tools.models, model_name)(enc_dims)

    with open(os.path.join(input_modelpath, "weights.pt"), "rb") as fp:
        state_dict = torch.load(fp, map_location=torch.device("cpu"))
    model.load_state_dict(state_dict, strict=False)
    model.to(device)
    model.eval()

    # Get recommendations
    input_dim = enc_dims[0]
    user_input = create_user_input(input_dim, interactions, user_id, item_to_ind)
    recos = predict_for_user_id(user_input, model, k, ind_to_item)

    # Print names of recommended items
    print("-" * 89)
    recos_names: list[str] = list(
        interactions.set_index(Columns.Item).loc[recos, Columns.Name].unique()
    )
    print(*recos_names, sep="\n")
    print("-" * 89)


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
