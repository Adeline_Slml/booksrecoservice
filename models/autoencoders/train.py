import os
import pickle
import sys
import warnings
from typing import Literal, TypeAlias

import click
import mlflow
import numpy as np
import tools.models
import torch
import torch.nn.functional as F
from dotenv import load_dotenv
from mlflow.models.signature import infer_signature
from numpy.typing import NDArray
from tools.data import (
    BookTestDataset,
    BookTrainDataset,
    test_sparse_collate_fn,
    train_sparse_collate_fn,
)
from tools.metrics import map_at_k, mean_precision_at_k, mean_recall_at_k, mrr_at_k
from tools.models import DenoisingAE, LinearAE, VariationalAE
from tqdm import tqdm

warnings.filterwarnings("ignore")

Model: TypeAlias = DenoisingAE | LinearAE | VariationalAE
ModelName: TypeAlias = Literal["DenoisingAE", "LinearAE", "VariationalAE"]
Params: TypeAlias = dict[str, int | str | float | list[int]]
Optimizer: TypeAlias = torch.optim.Adam | torch.optim.SGD | torch.optim.RMSprop

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)
mlflow.set_experiment("AEs")

#################################################################################
# TRAINING CODE                                                                 #
#################################################################################


class Callback:
    """Log train and test results with help of MLFlow.

    Attributes:
        dataset: The test dataset.
        batch_size: The size of one batch.
        delimeter: The number of steps agter which _testing will be called.
        k: The number of recos which are considered optimal to give.
        loss_name: The name of loss function.

    """

    def __init__(
        self,
        dataset: BookTestDataset,
        batch_size: int,
        delimeter: int,
        k: int,
        loss_name: str,
    ):
        self.step = 0
        self.delimeter = delimeter
        self.batch_size = batch_size
        self.dataset = dataset
        self.k = k
        self.loss_name = loss_name

    def forward(self, model: Model, loss: float) -> None:
        """Save losses and metrics values to MLFlow."""
        self.step += 1
        mlflow.log_metrics({f"Losses/{self.loss_name}_train": loss}, self.step)

        if self.step % self.delimeter == 0:
            test_loss, recall, precision, mrr, map_ = self._testing(model)
            scores = {
                f"Losses/{self.loss_name}_test": test_loss,
                f"Metrics/Recall_{self.k}": recall,
                f"Metrics/Precision_{self.k}": precision,
                f"Metrics/MRR_{self.k}": mrr,
                f"Metrics/MAP_{self.k}": map_,
            }
            mlflow.log_metrics(scores, self.step)

    def _testing(self, model: Model) -> tuple[float, float, float, float, float]:
        total_loss = 0.0
        recall_list: list[float] = []
        precision_list: list[float] = []
        mrr_list: list[float] = []
        map_list: list[float] = []

        for batch_input, batch_valid in torch.utils.data.DataLoader(
            dataset=self.dataset,
            batch_size=self.batch_size,
            shuffle=False,
            collate_fn=test_sparse_collate_fn,
        ):
            batch_input = batch_input.to(model.device)
            if self.loss_name == "MSE":
                batch_recon: torch.Tensor = model(batch_input)
                reduced_loss = calculate_mse_loss(
                    batch_recon, batch_valid.to(model.device)
                )
            else:
                batch_recon, mu, logvar = model(batch_input)
                reduced_loss = calculate_elbo_loss(batch_recon, batch_input, mu, logvar)
            total_loss += reduced_loss.to("cpu").item()
            # Convert to numpy
            batch_recon_np: NDArray[np.float32] = batch_recon.to("cpu").numpy()
            batch_valid_np: NDArray[np.float32] = batch_valid.numpy()
            batch_input_np: NDArray[np.float32] = batch_input.to("cpu").numpy()

            # Exclude known ratings that were used to build reconstruction
            batch_recon_np[batch_input_np.nonzero()] = float("-inf")

            # Compute metrics
            recall = mean_recall_at_k(batch_recon_np, batch_valid_np, self.k)
            precision = mean_precision_at_k(batch_recon_np, batch_valid_np, self.k)
            mrr = mrr_at_k(batch_recon_np, batch_valid_np, self.k)
            map_ = map_at_k(batch_recon_np, batch_valid_np, self.k)
            recall_list.append(recall)
            precision_list.append(precision)
            mrr_list.append(mrr)
            map_list.append(map_)

        return (
            total_loss,
            sum(recall_list) / len(recall_list),
            sum(precision_list) / len(precision_list),
            sum(mrr_list) / len(mrr_list),
            sum(map_list) / len(map_list),
        )

    def __call__(self, model: Model, loss: float):
        return self.forward(model, loss)


def calculate_elbo_loss(
    batch_recon: torch.Tensor,
    batch: torch.Tensor,
    mu: torch.Tensor,
    logvar: torch.Tensor,
) -> torch.Tensor:
    """Calculate the ELBO approximation of log-likelihood with negative sign."""
    cross_entropy = torch.sum(F.log_softmax(batch_recon, dim=1) * batch, dim=1)
    kl_divergence = 0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp(), dim=1)
    return -1 * torch.mean(cross_entropy + kl_divergence)


def calculate_mse_loss(batch_recon: torch.Tensor, batch: torch.Tensor) -> torch.Tensor:
    """Calculate the MSE loss."""
    return F.mse_loss(batch_recon, batch)


def train_on_batch(model: Model, batch: torch.Tensor, optimizer: Optimizer) -> float:
    """Train model on only one batch of data."""
    model.train()
    optimizer.zero_grad()
    batch = batch.to(model.device)

    if not isinstance(model, VariationalAE):
        batch_recon = model(batch)
        reduced_loss = calculate_mse_loss(batch_recon, batch)
    else:
        batch_recon, mu, logvar = model(batch)
        reduced_loss = calculate_elbo_loss(batch_recon, batch, mu, logvar)
    reduced_loss.backward()  # type: ignore
    optimizer.step()

    return reduced_loss.to("cpu").item()


def train_epoch(
    train_generator: tqdm,
    model: Model,
    optimizer: Optimizer,
    callback: None | Callback,
) -> float:
    """Train model on only one epoch."""
    total_len, epoch_loss = 0, 0.0
    for batch in train_generator:
        batch_loss = train_on_batch(model, batch, optimizer)

        if callback:
            model.eval()
            with torch.no_grad():
                callback(model, batch_loss)

        epoch_loss += batch_loss * len(batch)
        total_len += len(batch)
    return epoch_loss / total_len


def trainer(
    num_epoch: int,
    batch_size: int,
    dataset: BookTrainDataset,
    model: Model,
    optimizer_name: Literal["Adam", "SGD", "RMSprop"],
    lr: float,
    wd: float,
    callback: None | Callback,
) -> None:
    """Provide training for choosed number of epochs."""
    optimizer = getattr(torch.optim, optimizer_name)(
        model.parameters(), lr=lr, weight_decay=wd
    )

    iterations = tqdm(range(num_epoch), desc="epoch")
    iterations.set_postfix({"train epoch loss": np.nan})
    for _ in iterations:
        batch_generator = tqdm(
            torch.utils.data.DataLoader(
                dataset=dataset,
                batch_size=batch_size,
                shuffle=True,
                collate_fn=train_sparse_collate_fn,
            ),
            leave=False,
            total=len(dataset) // batch_size + (len(dataset) % batch_size > 0),
        )

        epoch_loss = train_epoch(
            batch_generator,
            model,
            optimizer,
            callback,
        )

        iterations.set_postfix({"train epoch loss": epoch_loss})


def save_model_data(
    model: Model, model_name: ModelName, enc_dims: list[int], output_modelpath: str
) -> None:
    """Save data for model initialization within inference."""
    if not os.path.exists(output_modelpath):
        os.makedirs(output_modelpath)

    output_namepath = os.path.join(output_modelpath, "name.pickle")
    with open(output_namepath, "wb") as fp:
        pickle.dump(model_name, fp)

    output_dimspath = os.path.join(output_modelpath, "dims.pickle")
    with open(output_dimspath, "wb") as fp:
        pickle.dump(enc_dims, fp)

    output_modelpath = os.path.join(output_modelpath, "weights.pt")
    with open(output_modelpath, "wb") as fp:
        torch.save(model.state_dict(), fp)


#################################################################################
# TRAINING STAGE                                                                #
#################################################################################


@click.command()
@click.pass_context
@click.option(
    "--input-datapath",
    default="models/autoencoders/data",
    type=click.Path(exists=True),
    help="Path where saved data for training & inference is allocated. Change default "
    "if you only specified another one during data creation",
)
@click.option(
    "--output-modelpath",
    default="models/autoencoders/model",
    type=click.Path(),
    help="Path where you want to save model data. Change default if you only need to",
)
@click.option(
    "--manual-seed",
    default=123,
    type=int,
    show_default=True,
    help="Random seed for reproducibility",
)
@click.option(
    "--num-epoch",
    default=1,
    type=int,
    show_default=True,
    help="Total number of epochs",
)
@click.option(
    "--batch-size", default=1028, type=int, show_default=True, help="Batch size"
)
@click.option(
    "--model",
    "model_name",
    default="VariationalAE",
    type=click.Choice(["DenoisingAE", "LinearAE", "VariationalAE"]),
    help="Optimization algorithm",
    show_default=True,
)
@click.option(
    "--enc-dims",
    multiple=True,
    default=[600, 200],
    help="Dimensions sizes of encoder. For instance, default [600, 200] would be "
    "redefined as [h_i, 600, 200], where the input size h_i is pulled out from "
    "the data automatically, 600 is hidden layer size, 200 is the the latent dim. "
    "Decoder is symmetric, i.e. [200, 600, h_i]",
    show_default=True,
)
@click.option(
    "--optimizer",
    "optimizer_name",
    default="Adam",
    type=click.Choice(["Adam", "SGD", "RMSprop"]),
    help="Optimization algorithm",
    show_default=True,
)
@click.option(
    "--lr", default=0.001, type=float, show_default=True, help="Learning rate"
)
@click.option(
    "--wd", default=0.0, type=float, show_default=True, help="Weight decay (L2 penalty)"
)
@click.option(
    "--cuda",
    is_flag=True,
    help="Execution device. Don't specify --cuda if cpu is preffered",
)
@click.option(
    "--call",
    is_flag=True,
    help="Whether the training results will be logged in MLFlow or not",
)
@click.option(
    "--num-recos",
    "k",
    default=15,
    type=int,
    show_default=True,
    help="Number of recos to give user",
)
def main(
    ctx: click.Context,
    input_datapath: str,
    output_modelpath: str,
    manual_seed: int,
    num_epoch: int,
    batch_size: int,
    model_name: Literal["DenoisingAE", "LinearAE", "VariationalAE"],
    enc_dims: list[int],
    optimizer_name: Literal["Adam", "SGD", "RMSprop"],
    lr: float,
    wd: float,
    cuda: bool,
    call: bool,
    k: int,
) -> None:
    """Run training pipeline."""
    # Set random seeds
    torch.manual_seed(manual_seed)

    # Set device
    if torch.cuda.is_available() and not cuda:
        print(
            "WARNING: You have a CUDA device, so you should probably run with --cuda."
        )
    elif not torch.cuda.is_available() and cuda:
        print("WARNING: You don't have a CUDA device, so the program will run on cpu.")
    device = torch.device("cuda" if cuda and torch.cuda.is_available() else "cpu")

    # Load everything necessary
    train_dataset = BookTrainDataset(path=input_datapath)
    test_dataset = BookTestDataset(path=input_datapath)

    print(f"Train matrix sparsity: {train_dataset.sparsity}", end="\n")
    print(
        f"Test matrix (as model input) sparsity: {test_dataset.sparsity[0]}", end="\n"
    )
    print(
        f"Test matrix (as holdout for metric evaluation) sparsity: "
        f"{test_dataset.sparsity[1]}"
    )

    # Initialize model
    input_dim = train_dataset.n_books

    # Build proper model dims list
    enc_dims = list(enc_dims)
    enc_dims.insert(0, input_dim)

    model: Model = getattr(tools.models, model_name)(enc_dims)
    _ = model.to(device)
    loss_name = "ELBO" if isinstance(model, VariationalAE) else "MSE"

    # Save params and initialize Callback
    callback = None
    if call:
        params: Params = {}
        args = [
            "num_epoch",
            "batch_size",
            "enc_dims",
            "model_name",
            "optimizer_name",
            "lr",
            "wd",
        ]

        for i in args:
            params[i] = locals()[i]

        mlflow.start_run()
        mlflow.log_params(params)

        # Validation will be called after each epoch
        delimeter = len(train_dataset) // batch_size + (
            len(train_dataset) % batch_size > 0
        )
        callback = Callback(test_dataset, batch_size, delimeter, k, loss_name)

    # Training
    try:
        print("\nTraining starts. You could exit training stage by pressing CRTL-C.")
        trainer(
            num_epoch,
            batch_size,
            train_dataset,
            model,
            optimizer_name,
            lr,
            wd,
            callback,
        )
        print("Training ends.")
    except KeyboardInterrupt:
        print("-" * 89)
        print("Training was terminated.")
        try:
            sys.exit(130)
        except SystemExit:
            os._exit(130)

    # Log the model to MLFlow
    if call:
        # Create model signature
        dummy_loader = torch.utils.data.DataLoader(
            dataset=test_dataset,
            batch_size=batch_size,
            shuffle=False,
            collate_fn=test_sparse_collate_fn,
        )
        batch_input, _ = next(iter(dummy_loader))
        signature = infer_signature(
            batch_input.numpy(), model.predict(batch_input).detach().to("cpu").numpy()
        )
        # Log the model
        mlflow.pytorch.log_model(
            pytorch_model=model,
            artifact_path="model",
            signature=signature,
            registered_model_name="ae_model",
        )
        mlflow.end_run()

    save_model_data(model, model_name, enc_dims, output_modelpath)
    print("Model data saved locally.")


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
