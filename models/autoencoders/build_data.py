"""Process the data pipeline."""
import os
import pickle
import sys

import click
import numpy as np
import pandas as pd
from numpy.typing import NDArray
from tools.data import (
    Columns,
    apply_reindex,
    filter_by_limits,
    reindex_mappings,
    split_test_data,
)


@click.command()
@click.pass_context
@click.option(
    "--input-filepath",
    default="data/processed",
    show_default=True,
    type=click.Path(exists=True),
    help="Path where the interactions data is allocated. Change default if "
    "you only need to",
)
@click.option(
    "--output-datapath",
    default="models/autoencoders/data",
    show_default=True,
    type=click.Path(),
    help="Path where you want to save data for training & inference. "
    "Change default if you only need to",
)
@click.option(
    "--user-lim",
    default=4,
    type=int,
    show_default=True,
    help="Mininum number of rated items that one user must have. "
    "Logic behind: more data for user - probably better reconstruction "
    "and better recos but you will loose some data. Play with that",
)
@click.option(
    "--item-lim",
    default=3,
    type=int,
    show_default=True,
    help="Mininum number of ratings that one item must have. "
    "Logic behind: if item have a few ratings it would be difficult "
    "to understand for whom this item is. Play with that",
)
@click.option(
    "--test-size",
    default=0.2,
    type=float,
    show_default=True,
    help="Float in (0, 1) range. Represents a proportion of test users",
)
@click.option(
    "--validation-size",
    default=0.5,
    type=float,
    show_default=True,
    help="Float in (0, 1) range. Represents a min proportion of items that "
    "will be used for metrics calculation",
)
@click.option(
    "--manual-seed",
    default=123,
    type=int,
    show_default=True,
    help="Random seed for reproducibility",
)
@click.option(
    "--save-data",
    is_flag=True,
    default=True,
    show_default=True,
    help="Whether simply drop test users that have no required amount of relevance "
    "items or save them by including into train set",
)
def main(  # pylint: disable=too-many-locals
    ctx: click.Context,
    input_filepath: str,
    output_datapath: str,
    user_lim: int,
    item_lim: int,
    test_size: float,
    validation_size: float,
    manual_seed: int,
    save_data: bool,
) -> None:
    """Generate proper data for building datasets in training pipeline after."""
    # Handling errors
    if not 0 < test_size < 1:
        raise ValueError(f"test_size={test_size} should be a float in (0, 1) range.")
    if not 0 < validation_size < 1:
        raise ValueError(
            f"validation_size={validation_size} should be a float in (0, 1) range."
        )

    # Set random seed for reproducibility
    np.random.seed(manual_seed)

    print("Load and preprocess books dataset.")
    # Load data
    try:
        interactions = pd.read_csv(
            os.path.join(input_filepath, "user_rating.csv"), header=0
        )
    except FileNotFoundError:
        print("You must download the interactions dataframe first.")
        sys.exit(1)

    interactions.rename(
        columns={
            "User_Id": Columns.User,
            "Book_Id": Columns.Item,
            "Rating": Columns.Weight,
        },
        inplace=True,
    )

    # Filter and normalize data
    interactions, user_groups, _ = filter_by_limits(interactions, user_lim, item_lim)

    maxx, minn = interactions[Columns.Weight].max(), interactions[Columns.Weight].min()
    interactions[Columns.Weight] = (interactions[Columns.Weight] - minn) / (maxx - minn)

    # Get users/items indices
    unique_users_id: NDArray[np.int64] = user_groups[Columns.User].values
    n_users = len(unique_users_id)

    # Shuffle indices
    np.random.shuffle(unique_users_id)

    n_heldout_users = int(test_size * n_users)

    # Split into train/test folds by indices
    train_users = unique_users_id[: (n_users - n_heldout_users)]
    print(f"Train fold size is: {len(train_users)}")
    test_users = unique_users_id[(n_users - n_heldout_users) :]
    print(f"Test fold size is: {len(test_users)}")

    # Prepare train/test data for PyTorch dataset
    train_df = interactions.loc[interactions[Columns.User].isin(train_users)]
    # Save items presented in train_df only
    unique_items_id = pd.unique(train_df[Columns.Item])
    # Prepare test data for PyTorch dataset
    test_df = interactions.loc[interactions[Columns.User].isin(test_users)]
    test_df = test_df.loc[test_df[Columns.Item].isin(unique_items_id)]

    # Split the test dataframe into two parts
    relevance_trsh = 3.5  # Minimum rating by which an item is considered relevant
    norm_relevance_trsh = (relevance_trsh - minn) / (maxx - minn)
    test_df_input, test_df_valid, rejected_test_users = split_test_data(
        test_df, validation_size, norm_relevance_trsh
    )
    ratio_lost = len(rejected_test_users) / len(test_users)
    print(
        f"WARNING: {(100 * ratio_lost):.2f} % of the test users were lost due to "
        "the absence of the required amount of relevant items.",
        end=" ",
    )

    if save_data and rejected_test_users:
        # Transfer the discarded test users to train and generate all data once again
        train_df = pd.concat(
            [
                train_df,
                interactions.loc[interactions[Columns.User].isin(rejected_test_users)],
            ]
        )
        unique_items_id = pd.unique(train_df[Columns.Item])
        test_users_set = set(test_users) - set(rejected_test_users)
        test_df = interactions.loc[interactions[Columns.User].isin(test_users_set)]
        test_df = test_df.loc[test_df[Columns.Item].isin(unique_items_id)]

        test_df_input, test_df_valid, rejected_test_users = split_test_data(
            test_df, validation_size, norm_relevance_trsh
        )
        print("They were successfully included into the train data")
    elif rejected_test_users and not save_data:
        # Simply drop these users
        test_df = test_df.loc[~test_df[Columns.User].isin(rejected_test_users)]
        print("They were completely dropped from the data")
    print(f"Final train fold size is: {len(pd.unique(train_df[Columns.User]))}")
    print(f"Final test fold size is: {len(test_users) - len(rejected_test_users)}")

    # Prepare inference data
    inference_df = interactions.loc[interactions[Columns.Item].isin(unique_items_id)]
    inference_users = inference_df[Columns.User].unique()

    # Get mappings
    user_to_ind, item_to_ind = reindex_mappings(train_df, test_df, unique_items_id)

    # Apply inplace remapping
    for df in (train_df, test_df_input, test_df_valid):
        apply_reindex(df, user_to_ind, item_to_ind)

    # Save data
    if not os.path.exists(output_datapath):
        os.makedirs(output_datapath)

    with open(os.path.join(output_datapath, "item_to_ind"), "wb") as fp:
        pickle.dump(item_to_ind, fp)
    train_df.to_csv(os.path.join(output_datapath, "train.csv"), index=False)
    test_df_input.to_csv(os.path.join(output_datapath, "test_input.csv"), index=False)
    test_df_valid.to_csv(os.path.join(output_datapath, "test_valid.csv"), index=False)
    inference_df.to_csv(os.path.join(output_datapath, "inference.csv"), index=False)

    with open(
        os.path.join(output_datapath, "inference_users.txt"), "w", encoding="utf8"
    ) as f:
        for user_id in inference_users:
            f.write(f"{user_id}\n")

    items_count = len(unique_items_id)
    with open(
        os.path.join(output_datapath, "items_count.txt"), "w", encoding="utf8"
    ) as f:
        f.write(f"{items_count}")

    print("Done!")


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
